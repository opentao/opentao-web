DROP TABLE IF EXISTS SHAPE_POINT;
DROP TABLE IF EXISTS STOP_TIME;
DROP TABLE IF EXISTS TRIP;
DROP TABLE IF EXISTS SHAPE;
DROP TABLE IF EXISTS ROUTE;
DROP TABLE IF EXISTS CALENDAR_DATE;
DROP TABLE IF EXISTS CALENDAR;
DROP TABLE IF EXISTS STOP;

-- See https://developers.google.com/transit/gtfs/reference#stopstxt
CREATE TABLE STOP
(
    stopId                 VARCHAR(256) PRIMARY KEY,
    stopName               TEXT,
    stopLatitude           REAL,
    stopLongitude          REAL,
    stopLocationType       TINYINT,
    stopParentStationId    VARCHAR(256),
    stopTimeZone           TEXT,
    stopWheelchairBoarding TINYINT,
    stopPlatformCode       TEXT,
    CONSTRAINT FK_STOP_PARENT_STOP FOREIGN KEY (stopParentStationId) REFERENCES STOP (stopId)
) ENGINE = InnoDB;

-- See https://developers.google.com/transit/gtfs/reference#calendartxt
CREATE TABLE CALENDAR
(
    calendarServiceId VARCHAR(256) PRIMARY KEY,
    calendarMonday    TINYINT,
    calendarTuesday   TINYINT,
    calendarWednesday TINYINT,
    calendarThursday  TINYINT,
    calendarFriday    TINYINT,
    calendarSaturday  TINYINT,
    calendarSunday    TINYINT,
    calendarStartDate VARCHAR(8),
    calendarEndDate   VARCHAR(8)
) ENGINE = InnoDB;

-- See https://developers.google.com/transit/gtfs/reference#calendar_datestxt
CREATE TABLE CALENDAR_DATE
(
    calendarDateServiceId     VARCHAR(256) PRIMARY KEY,
    calendarDateDate          VARCHAR(8),
    calendarDateExceptionType TINYINT
) ENGINE = InnoDB;

CREATE TABLE ROUTE
(
    routeId        VARCHAR(256) PRIMARY KEY,
    routeShortName TINYTEXT,
    routeLongName  TINYTEXT,
    routeType      TINYINT NOT NULL,
    routeColor     VARCHAR(6),
    routeTextColor VARCHAR(6)
) ENGINE = InnoDB;

CREATE TABLE SHAPE
(
    shapeId VARCHAR(256) NOT NULL PRIMARY KEY
) ENGINE = InnoDB;

-- See https://developers.google.com/transit/gtfs/reference#tripstxt
CREATE TABLE TRIP
(
    tripId          VARCHAR(256) NOT NULL PRIMARY KEY,
    tripRouteId     VARCHAR(256) NOT NULL,
    tripServiceId   VARCHAR(256) NOT NULL,
    tripDirectionId TINYINT,
    tripShapeId     VARCHAR(256),
    tripCanceled    BOOLEAN      NOT NULL DEFAULT FALSE,
    CONSTRAINT FK_TRIP_ROUTE FOREIGN KEY (tripRouteId) REFERENCES ROUTE (routeId),
    CONSTRAINT FK_TRIP_SHAPE FOREIGN KEY (tripShapeId) REFERENCES SHAPE (shapeId)
) ENGINE = InnoDB;

-- See https://developers.google.com/transit/gtfs/reference#stop_timestxt
CREATE TAbLE STOP_TIME
(
    stopTimeTripId               VARCHAR(256) NOT NULL,
    stopTimeArrivalTime          TIME,
    stopTimeDepartureTime        TIME,
    stopTimeStopId               VARCHAR(256) NOT NULL,
    stopTimeStopSequence         INT          NOT NULL,
    stopTimePickupType           TINYINT      NOT NULL,
    stopTimeSkipped              BOOLEAN      NOT NULL DEFAULT FALSE,
    stopTimeArrivalUncertainty   INT          NOT NULL DEFAULT 0.0,
    stopTimeDepartureUncertainty INT          NOT NULL DEFAULT 0.0,
    PRIMARY KEY (stopTimeTripId, stopTimeStopId, stopTimeStopSequence),
    CONSTRAINT FK_STOP_TIME_TRIP FOREIGN KEY (stopTimeTripId) REFERENCES TRIP (tripId),
    CONSTRAINT FK_STOP_TIME_STOP FOREIGN KEY (stopTimeStopId) REFERENCES STOP (stopId)
) ENGINE = InnoDB;

CREATE TABLE SHAPE_POINT
(
    shapePointShapeId   VARCHAR(256) NOT NULL,
    shapePointSequence  INT          NOT NULL,
    shapePointLatitude  REAL         NOT NULL,
    shapePointLongitude REAL         NOT NULL,
    PRIMARY KEY (shapePointShapeId, shapePointSequence),
    CONSTRAINT FK_SHAPE_POINT_SHAPE FOREIGN KEY (shapePointShapeId) REFERENCES SHAPE (shapeId)
) ENGINE = InnoDB;

CREATE TABLE GTFS_UPDATE
(
    lastGtfsUpdate DATETIME NOT NULL
);