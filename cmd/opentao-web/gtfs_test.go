package main

import (
	"archive/zip"
	"bufio"
	"context"
	"database/sql"
	"encoding/csv"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

var testDir string = ""

func TestMain(m *testing.M) {
	log.Println("Setuping DB")

	databaseUrl, databaseUrlExist := os.LookupEnv("DATABASE_URL_TEST")
	if !databaseUrlExist {
		log.Fatalln("MISSING DATABASE_URL_TEST env var")
	}

	log.Println(databaseUrl)

	db, err := sql.Open("mysql", databaseUrl)
	if err != nil {
		log.Fatalf("Failed to open database : %v", err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	client := &http.Client{}
	ctx := context.Background()

	testDir, err = os.MkdirTemp("", "gtfs-test-*")
	if err != nil {
		log.Fatalf("Failed to open testDir : %v", err)
	}

	defer os.RemoveAll(testDir)

	gtfsFile, size, err := downloadFile(client)
	if err != nil {
		log.Fatalf("Failed to open testDir : %v", err)
	}
	defer gtfsFile.Close()
	defer os.Remove(gtfsFile.Name())

	r, err := zip.NewReader(gtfsFile, size)
	if err != nil {
		log.Fatalln(err)
	}

	for _, s := range r.File {
		f, err := os.Create(filepath.Join(testDir, s.Name))
		if err != nil {
			log.Fatalln(err)
		}

		w := bufio.NewWriter(f)
		r, err := s.Open()
		if err != nil {
			_ = f.Close()
			log.Fatalln(err)
		}
		_, err = io.Copy(w, r)
		_ = f.Close()

		if err != nil {
			log.Fatalln(err)
		}
	}

	if err := readGtfs(client, db, ctx); err != nil {
		log.Fatalf("Failed to populate database : %v", err)
	}

	exitValue := m.Run()
	os.Exit(exitValue)
}

func TestDb_StopTimes_AllInDb(t *testing.T) {
	f, err := os.Open(filepath.Join(testDir, "stop_times.txt"))
	if err != nil {
		t.Fatal(err)
	}

	input := bufio.NewReader(f)

	csvReader := csv.NewReader(input)

	headers, err := csvReader.Read()
	if err != nil {
		t.Fatal(err)
	} else {
		if headers[0] != "trip_id" || headers[1] != "arrival_time" || headers[2] != "departure_time" || headers[3] != "stop_id" || headers[4] != "stop_sequence" || headers[5] != "pickup_type" {
			t.Fatalf("Invalide headers : %v", headers)
		}
	}

	databaseUrl, databaseUrlExist := os.LookupEnv("DATABASE_URL_TEST")
	if !databaseUrlExist {
		t.Fatal("MISSING DATABASE_URL_TEST env var")
	}

	db, err := sql.Open("mysql", databaseUrl)
	if err != nil {
		t.Fatalf("Failed to open database : %v", err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	for {
		content, err := csvReader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				t.Fatal(err)
			}
		}

		stopTimeSequence, err := strconv.Atoi(content[4])
		if err != nil {
			t.Fatal(err)
		}

		stopTimePickupType, err := strconv.Atoi(content[5])
		if err != nil {
			t.Fatal(err)
		}

		rows, err := db.Query(
			"SELECT 'foo' FROM STOP_TIME WHERE stopTimeTripId = ? AND stopTimeArrivalTime = ? AND stopTimeDepartureTime = ? and stopTimeStopId = ? and stopTimeStopSequence = ? AND stopTimePickupType = ?",
			content[0],
			content[1],
			content[2],
			content[3],
			stopTimeSequence,
			stopTimePickupType,
		)

		if err != nil {
			log.Fatal(err)
		} else {
			if !rows.Next() {
				t.Fatalf("Not found, %v", content)
			} else {
				rows.Close()
			}
		}
	}
}
