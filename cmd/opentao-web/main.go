package main

import (
	"context"
	"database/sql"
	"github.com/rs/zerolog"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/MobilityData/gtfs-realtime-bindings/golang/gtfs"
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

type BusPosition struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
	RouteId   string  `json:"route_id"`
	VehicleId string  `json:"vehicle_id"`
	Timestamp uint64  `json:"timestamp"`
}

const Debug = true

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	err := godotenv.Load()
	if err != nil {
		log.Fatal().Msg("Error loading .env file")
	}

	databaseUrl, databaseUrlExist := os.LookupEnv("DATABASE_URL")
	if !databaseUrlExist {
		log.Fatal().Msg("Missing DATABASE_URL env var")
	}

	db, err := sql.Open("mysql", databaseUrl)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to open database")
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	client := &http.Client{}
	ctx := context.Background()

	log.Info().Msg("Populating database ...")
	if err := readGtfs(client, db, ctx); err != nil {
		log.Fatal().Err(err).Msg("Failed to populate database")
	}

	gin.DebugPrintRouteFunc = func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
		log.Debug().
			Str("method", httpMethod).
			Str("path", absolutePath).
			Str("handler", handlerName).
			Int("nbrHandlers", nuHandlers).
			Send()
	}

	r := gin.Default()
	r.GET("/api", func(c *gin.Context) {
		if err := serveApi(c, client); err != nil {
			log.Fatal().Err(err).Msg("Failed to serve api")

			c.String(500, "Internal Server Error")
		}
	})

	if Debug {
		r.StaticFile("/", "./site/index.html")
		r.StaticFile("/favicon.ico", "./site/favicon.ico")
		r.StaticFS("/static/", http.Dir("./site/static/"))
	}

	if err := r.Run("0.0.0.0:8050"); err != nil {
		return
	}
}

func serveApi(c *gin.Context, client *http.Client) error {
	req, err := http.NewRequest("GET", "https://ara-api.enroute.mobi/tao/gtfs/vehicle-positions", nil)
	resp, err := client.Do(req)
	defer resp.Body.Close()

	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	feed := gtfs.FeedMessage{}
	err = proto.Unmarshal(body, &feed)
	if err != nil {
		return err
	}

	result := make([]BusPosition, len(feed.Entity))

	for i, entity := range feed.Entity {
		vehiclePosition := entity.Vehicle

		result[i].Latitude = *vehiclePosition.Position.Latitude
		result[i].Longitude = *vehiclePosition.Position.Longitude
		result[i].RouteId = *vehiclePosition.Trip.RouteId
		result[i].VehicleId = *vehiclePosition.Vehicle.Id
		result[i].Timestamp = *vehiclePosition.Timestamp
	}

	c.JSON(200, result)
	return nil
}
