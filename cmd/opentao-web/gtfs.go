package main

import (
	"archive/zip"
	"bufio"
	"context"
	"database/sql"
	"encoding/csv"
	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const BufferSize = 2000

func sanitize(value string) *string {
	if value != "" {
		return &value
	} else {
		return nil
	}
}

func readGtfs(client *http.Client, db *sql.DB, ctx context.Context) error {
	input, size, err := downloadFile(client)
	if err != nil {
		return err
	}
	defer input.Close()
	defer os.Remove(input.Name())

	r, err := zip.NewReader(input, size)
	if err != nil {
		return err
	}

	f, err := r.Open("stop_times.txt")
	if err != nil {
		return err
	}

	s, err := f.Stat()
	if err != nil {
		return err
	}

	modTime := s.ModTime()

	row := db.QueryRowContext(ctx, "SELECT lastGtfsUpdate != ? FROM GTFS_UPDATE", modTime)

	reload := true
	if err := row.Scan(&reload); err != nil && err != sql.ErrNoRows {
		return err
	}

	if !reload {
		log.Debug().Msg("Database already up to date")
		return nil
	}

	_ = f.Close()

	dir, err := os.MkdirTemp("", "gtfs-*")
	if err != nil {
		return err
	}

	defer os.RemoveAll(dir)

	for _, s := range r.File {
		f, err := os.Create(filepath.Join(dir, s.Name))
		if err != nil {
			return err
		}

		w := bufio.NewWriter(f)
		r, err := s.Open()
		if err != nil {
			_ = f.Close()
			return err
		}
		_, err = io.Copy(w, r)
		_ = f.Close()

		if err != nil {
			return err
		}
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "SET autocommit = 0"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "SET FOREIGN_KEY_CHECKS=0"); err != nil {
		return err
	}

	if err := deleteTables(tx, ctx); err != nil {
		return err
	}

	if err := importData(dir, ctx, tx); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "SET autocommit = 1"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "SET FOREIGN_KEY_CHECKS=1"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE GTFS_UPDATE"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "INSERT INTO GTFS_UPDATE(lastGtfsUpdate) VALUES(?)", modTime); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func importData(dir string, ctx context.Context, tx *sql.Tx) error {
	if err := readFile(dir, "stops.txt", ctx, tx, readStops); err != nil {
		return err
	}

	if err := readFile(dir, "shapes.txt", ctx, tx, readShapes); err != nil {
		return err
	}

	if err := readFile(dir, "routes.txt", ctx, tx, readRoutes); err != nil {
		return err
	}

	if err := readFile(dir, "trips.txt", ctx, tx, readTrips); err != nil {
		return err
	}

	if err := readFile(dir, "stop_times.txt", ctx, tx, readStopTimes); err != nil {
		return err
	}
	return nil
}

func deleteTables(tx *sql.Tx, ctx context.Context) error {
	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE SHAPE_POINT"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE SHAPE_POINT"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE STOP_TIME"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE STOP_TIME"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE TRIP"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE TRIP"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE SHAPE"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE SHAPE"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE ROUTE"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE ROUTE"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "TRUNCATE TABLE STOP"); err != nil {
		return err
	}

	if _, err := tx.ExecContext(ctx, "OPTIMIZE TABLE STOP"); err != nil {
		return err
	}

	return nil
}

func readFile(basePath string, name string, ctx context.Context, tx *sql.Tx, callback func(reader *csv.Reader, ctx context.Context, tx *sql.Tx) error) error {
	filePath := filepath.Join(basePath, name)

	f, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	input := bufio.NewReader(f)
	csvReader := csv.NewReader(input)

	if _, err := csvReader.Read(); err != nil {
		return err
	}

	return callback(csvReader, ctx, tx)
}

func readRoutes(routesReader *csv.Reader, ctx context.Context, db *sql.Tx) error {
	stm, err := db.PrepareContext(ctx, "INSERT INTO ROUTE(routeId,routeShortName,routeLongName,routeType,routeColor, routeTextColor) VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}

	for {
		content, err := routesReader.Read()
		if err != nil {
			break
		}

		if _, err := stm.ExecContext(
			ctx,
			sanitize(content[0]),
			sanitize(content[2]),
			sanitize(content[3]),
			sanitize(content[4]),
			sanitize(content[5]),
			sanitize(content[6]),
		); err != nil {
			return err
		}
	}
	return nil
}

func readShapes(shapesReader *csv.Reader, ctx context.Context, db *sql.Tx) error {
	stmInsertShape, err := db.PrepareContext(ctx, "INSERT INTO SHAPE(shapeId) VALUES (?)")
	if err != nil {
		return err
	}

	stm, err := db.PrepareContext(ctx, "INSERT INTO SHAPE_POINT(shapePointShapeId,shapePointSequence,shapePointLatitude,shapePointLongitude) VALUES (?, ?, ?, ?)")
	if err != nil {
		return err
	}

	var shapeId *string = nil
	for {
		content, err := shapesReader.Read()
		if err != nil {
			break
		}

		if shapeId == nil || *shapeId != content[0] {
			if _, err := stmInsertShape.ExecContext(ctx, content[0]); err != nil {
				return err
			}
			shapeId = &content[0]
		}

		if _, err := stm.ExecContext(
			ctx,
			sanitize(content[0]),
			content[3],
			sanitize(content[1]),
			sanitize(content[2]),
		); err != nil {
			return err
		}
	}
	return nil

}

func readTrips(tripsReader *csv.Reader, ctx context.Context, db *sql.Tx) error {
	stm, err := db.PrepareContext(ctx, "INSERT INTO TRIP(tripId,tripRouteId,tripServiceId,tripDirectionId,tripShapeId) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}

	for {
		content, err := tripsReader.Read()
		if err != nil {
			break
		}

		if _, err := stm.ExecContext(
			ctx,
			sanitize(content[2]),
			sanitize(content[0]),
			sanitize(content[1]),
			sanitize(content[4]),
			sanitize(content[5]),
		); err != nil {
			return err
		}
	}
	return nil
}

func readStopTimes(stopTimesReader *csv.Reader, ctx context.Context, db *sql.Tx) error {
	stm, err := db.PrepareContext(ctx, "INSERT INTO STOP_TIME(stopTimeTripId,stopTimeArrivalTime,stopTimeDepartureTime,stopTimeStopId,stopTimeStopSequence,stopTimePickupType) VALUES (?, ?, ?, ?, ?, ?)"+strings.Repeat(",(?, ?, ?, ?, ?, ?)", BufferSize-1))
	if err != nil {
		return err
	}

	buffer := make([]any, 0, BufferSize*6)

	for {
		content, err := stopTimesReader.Read()
		if err != nil {
			break
		}

		buffer = append(buffer, sanitize(content[0]),
			sanitize(content[1]),
			sanitize(content[2]),
			sanitize(content[3]),
			sanitize(content[4]),
			sanitize(content[5]))

		if len(buffer) == BufferSize*6 {
			if _, err := stm.ExecContext(ctx, buffer...); err != nil {
				return err
			}
			buffer = make([]any, 0, BufferSize*6)
		}
	}

	if len(buffer) != 0 {
		stm, err := db.PrepareContext(ctx, "INSERT INTO STOP_TIME(stopTimeTripId,stopTimeArrivalTime,stopTimeDepartureTime,stopTimeStopId,stopTimeStopSequence,stopTimePickupType) VALUES (?, ?, ?, ?, ?, ?)")
		if err != nil {
			return err
		}

		index := 0
		for index < len(buffer) {
			if _, err := stm.ExecContext(ctx, buffer[index+0], buffer[index+1], buffer[index+2], buffer[index+3], buffer[index+4], buffer[index+5]); err != nil {
				return err
			}
			index += 6
		}
	}
	return nil
}

func readStops(stopsReader *csv.Reader, ctx context.Context, db *sql.Tx) error {
	stm, err := db.PrepareContext(ctx, "INSERT INTO STOP(stopId,stopName,stopLatitude,stopLongitude,stopLocationType,stopParentStationId,stopTimeZone,stopWheelchairBoarding,stopPlatformCode) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}

	for {
		content, err := stopsReader.Read()
		if err != nil {
			break
		}

		var stopLocationType *int = nil
		if content[4] != "" {
			stopLocationTypeValue, err := strconv.Atoi(content[4])
			if err != nil {
				return err
			}
			stopLocationType = &stopLocationTypeValue
		}

		var stopWheelchairBoarding *int = nil
		if content[7] != "" {
			stopWheelchairBoardingValue, err := strconv.Atoi(content[7])
			if err != nil {
				return err
			}
			stopWheelchairBoarding = &stopWheelchairBoardingValue
		}

		if _, err := stm.ExecContext(
			ctx,
			sanitize(content[0]),
			sanitize(content[1]),
			sanitize(content[2]),
			sanitize(content[3]),
			stopLocationType,
			sanitize(content[5]),
			sanitize(content[6]),
			stopWheelchairBoarding,
			sanitize(content[8])); err != nil {
			return err
		}

	}

	return nil
}

func downloadFile(client *http.Client) (*os.File, int64, error) {
	res, err := client.Get("https://chouette.enroute.mobi/api/v1/datas/keolis_orleans.gtfs.zip")
	if err != nil {
		return nil, -1, err
	}
	defer res.Body.Close()

	output, err := os.CreateTemp("", "gtfs-*.zip")
	if err != nil {
		return nil, -1, err
	}

	count, err := io.Copy(output, res.Body)
	if err != nil {
		output.Close()
		os.Remove(output.Name())
		return nil, -1, err
	}

	if _, err := output.Seek(0, 0); err != nil {
		output.Close()
		os.Remove(output.Name())
		return nil, -1, err
	}

	return output, count, nil
}
